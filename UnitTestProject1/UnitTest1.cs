﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestDevOps.Web.Controllers;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(7, MyClass.MyMethod(2, 4));
        }
    }
}
